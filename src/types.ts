export type ColorSchemeType = 'light' | 'dark'

export interface IMovie {
  adult: boolean
  backdrop_path?: string
  genre_ids: number[]
  id: number
  original_language: string
  original_title: string
  overview: string
  popularity: number
  poster_path?: string
  release_date: string
  title: string
  video: boolean
  vote_average: number
  vote_count: number
}

export interface IMovieAdditional {
  belongs_to_collection: undefined
  budget: number
  genres: Array<{id: number, name: string}>
  homepage?: string
  imdb_id?: string
  production_companies: Array<{id: number, name: string, logo_path?: string, origin_country: string}>
  production_countries: Array<{iso_3166_1: string, name: string}>
  revenue: number
  runtime?: number
  spoken_languages: Array<{iso_639_1: string, name: string}>
  status: 'Rumored' | 'Planned' | 'In Production' | 'Post Production' | 'Released' | 'Canceled'
  tagline?: string
}

export type IMovieFull = IMovie & IMovieAdditional