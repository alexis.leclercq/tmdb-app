import axios from "axios";
import { apiKey } from "../constants/TMDB";

export default function searchMovies(search: string) {
  return axios.get(`https://api.themoviedb.org/3/search/movie?api_key=${apiKey}&language=en-US&query=${search}&page=1&include_adult=false`)
}