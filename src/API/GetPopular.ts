import axios from "axios";
import { apiKey } from "../constants/TMDB";

export default function getPopularMovies() {
  return axios.get(`https://api.themoviedb.org/3/movie/popular?api_key=${apiKey}&language=en-US&page=1`)
}