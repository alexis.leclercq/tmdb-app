import axios from "axios";
import { apiKey } from "../constants/TMDB";

export default function getMovie(id: string) {
  return axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=${apiKey}&language=en-US&page=1&include_adult=false`)
}