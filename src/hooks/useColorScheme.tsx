import React, { useState, useRef, useContext, createContext } from 'react'
import { ColorSchemeType } from '../types';

export const ColorSchemeContext = createContext<{ colorScheme: ColorSchemeType, setColorScheme: (colorScheme: ColorSchemeType) => void }>({
  colorScheme: 'light',
  setColorScheme: (colorScheme: ColorSchemeType) => { }
});
export const useColorScheme = () => useContext(ColorSchemeContext);

export default function ColorSchemeProvider(props: any) {

  const [colorScheme, _setColorScheme] = useState<ColorSchemeType>('light');
  const colorSchemeRef = useRef(colorScheme);

  const setColorScheme = (val: ColorSchemeType) => {
    colorSchemeRef.current = val;
    _setColorScheme(val);
  }

  return (
    <ColorSchemeContext.Provider value={{
      colorScheme: colorSchemeRef.current,
      setColorScheme: setColorScheme
    }}>
      {props.children}
    </ColorSchemeContext.Provider>
  )
}