import React from 'react'
import styled from 'styled-components'
import { FaArrowLeft } from 'react-icons/fa';
import Colors from '../constants/Colors';
import ColorSchemeSelector from './ColorSchemeSelector';
import { ColorSchemeType } from '../types'
import { useColorScheme } from '../hooks/useColorScheme';
import { Link } from 'react-router-dom';

interface HeaderProps {
  title: string
}

const Wrapper = styled.header<{ colorScheme: ColorSchemeType }>`
  padding: 10px 0;
  color: white;
  display: flex;
  height: 5vh;
  ${({ colorScheme }) => `
    background: ${Colors[colorScheme].header};
  `}
`;

const Title = styled.h1`
  font-family: 'Roboto', sans-serif;
  font-size: 20px;
  text-align: center;
`;

const Div = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  :first-child > span { margin-right: auto; }
  :last-child > span { margin-left: auto;  }
`;

function Header({ title }: HeaderProps) {
  const { colorScheme } = useColorScheme()

  return (
    <Wrapper colorScheme={colorScheme}>
      {window.location.pathname !== '/' ? <Div>
        <Link to="/"><FaArrowLeft color={"#FFFFFF"} /></Link>
      </Div> : <Div />}
      <Div>
        <Title>
          {title}
        </Title>
      </Div>
      <Div>
        <ColorSchemeSelector />
      </Div>
    </Wrapper>
  )
}

export default Header