import styled from 'styled-components'
import Colors from '../constants/Colors';
import { ColorSchemeType } from '../types'

const Wrapper = styled.div<{ colorScheme: ColorSchemeType }>`
  min-height: 95vh;
  align-items: center;
  justify-center: center;
  text-align: center;
  @media only screen and (min-width: 769px) {
    padding: 1em 18vw;
  }
  @media only screen and (max-width: 768px) {
    padding: 0.5em 5px;
  }
  ${({ colorScheme }) => `
    background: ${Colors[colorScheme].background};
    color: ${Colors[colorScheme].backgroundText};
  `}
`;

export default Wrapper