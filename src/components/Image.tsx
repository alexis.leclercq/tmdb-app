import styled from 'styled-components'

const Image = styled.img`
  width: 140px;
  height: 190px;
  object-fit: cover;
`;

export default Image