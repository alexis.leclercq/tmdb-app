import styled from 'styled-components'

const Input = styled.input`
  position: relative;
  padding: 0.5em;
  box-sizing: border-box;
  border: 1px solid #dcdcdc;
  border-radius: 3px;
  background: white;
  color: black;
  -webkit-appearance: searchfield-cancel-button;
  @media only screen and (min-width: 769px) {
    margin: 1em 0;
    width: calc(100% - 2em);
  }
  @media only screen and (max-width: 768px) {
    margin: 5px;
    width: calc(100% - 10px);
  }
`;

export default Input