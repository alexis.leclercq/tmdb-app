import React from 'react'
import { Link } from 'react-router-dom';
import styled from 'styled-components'
import { IMovie } from '../types';
import Image from './Image';

interface ListProps {
  elements: IMovie[]
}

const Wrapper = styled.div`
  display: inline-flex;
  flex-wrap: wrap;
  justify-content: center;
  gap: 5px;
`;

const MovieBack = styled.div`
  position: relative;
`;

const Movie = styled.div`
  display: none;
  position: absolute;
  left: 0;
  top: 0;
  height: 100%;
  width: 100%;
  background: rgba(0, 0, 0, 0.5);
  justify-content: center;
  align-items: center;
  ${MovieBack}:hover & {
    display: block;
  }
`;

const Text = styled.p`
  color: #FFF;
  padding: 1em;
`;

function List({ elements }: ListProps) {

  return (
    <Wrapper>
      {elements.map((item, index) => (
        <Link to={`/movie/${item.id}`} key={index}>
          <MovieBack>
            {item.poster_path && <Image src={`https://image.tmdb.org/t/p/original/${item.poster_path}`} />}
            <Movie>
              <Text>
                {item.title}
              </Text>
            </Movie>
          </MovieBack>
        </Link>
      ))}
    </Wrapper>
  )
}

export default List