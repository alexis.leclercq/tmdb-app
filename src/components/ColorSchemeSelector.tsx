import React from 'react'
import styled from 'styled-components'
import { FaSun, FaMoon } from 'react-icons/fa';
import { useColorScheme } from '../hooks/useColorScheme';
import Switch from './Switch';

const Wrapper = styled.div`
  color: white;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const Spacing = styled.div`
  padding: 0 10px;
`;

function ColorSchemeSelector() {
  const { colorScheme, setColorScheme } = useColorScheme()

  return (
    <Wrapper>
      <Spacing>
        <FaSun />
      </Spacing>
      <Switch checked={colorScheme === "dark"} onChange={(e) => setColorScheme(e.target.checked ? 'dark' : 'light')} />
      <Spacing>
        <FaMoon />
      </Spacing>
    </Wrapper>
  )
}

export default ColorSchemeSelector