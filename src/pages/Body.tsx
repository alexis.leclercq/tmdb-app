import React, { useEffect, useState } from 'react'
import { IMovie } from '../types'
import { useColorScheme } from '../hooks/useColorScheme';
import List from '../components/List';
import getPopularMovies from '../API/GetPopular';
import searchMovies from '../API/SearchMovie';
import Wrapper from '../components/Wrapper';
import Input from '../components/Input';
import Header from '../components/Header';

function Body() {
  const { colorScheme } = useColorScheme()
  const [elements, setElements] = useState<IMovie[]>([])
  const [populars, setPopulars] = useState<IMovie[]>([])
  const [search, setSearch] = useState('')

  useEffect(() => {
    getPopularMovies().then(res => setPopulars(res.data.results))
  }, [])

  useEffect(() => {
    const delayDebounce = setTimeout(() => {
      if (search.length > 0)
        searchMovies(search).then(res => setElements(res.data.results))
    }, 500)
    return () => clearTimeout(delayDebounce)
  }, [search])

  return (
    <>
      <Header title="Movies" />
      <Wrapper colorScheme={colorScheme}>
        <Input placeholder="Rechercher un film" type="search" value={search} onChange={(e) => setSearch(e.target.value)} />
        <List elements={search.length > 0 ? elements : populars} />
      </Wrapper>
    </>
  )
}

export default Body