import React, { useEffect, useState } from 'react'
import styled from 'styled-components';
import { IMovieFull } from '../types'
import { useColorScheme } from '../hooks/useColorScheme';
import getMovie from '../API/getMovie';
import Wrapper from '../components/Wrapper';
import Header from '../components/Header';

const Row = styled.div`
  display: flex;
  
  @media only screen and (min-width: 769px) {
    flex-direction: row;
    justify-content: space-between;
  }
  @media only screen and (max-width: 768px) {
    flex-direction: column;
  }
`;

const Title = styled.h4`
  margin: 1em;
  text-align: left;
`;

const Text = styled.p`
  margin: 1em;
  text-align: left;
`;

const Image = styled.img`
  width: 180px;
  height: 250px;
  object-fit: cover;
  align-self: center;
`;

function Movie() {
  const { colorScheme } = useColorScheme()
  const [item, setItem] = useState<IMovieFull>()

  useEffect(() => {
    const id = window.location.pathname.substr(7)
    getMovie(id).then(res => setItem(res.data))
  }, [])

  return (
    <>
    <Header title={"Movies"} />
    <Wrapper colorScheme={colorScheme}>
      <Row>
        <div>
          <Title>{item?.title}</Title>
          <Text>{item?.overview}</Text>
          <Text>{item?.vote_average} / 10 (with {item?.vote_count} votes)</Text>
          <Text>Release: {item?.release_date}</Text>
          <Text>{item?.genres.map((genre, index) => (
            <span key={index}>{genre.name}{index !== item.genres.length - 1 && ', '}</span>
          ))}
          </Text>
        </div>
        {item?.poster_path && <Image src={`https://image.tmdb.org/t/p/original/${item.poster_path}`} />}
      </Row>
    </Wrapper>
    </>
  )
}

export default Movie