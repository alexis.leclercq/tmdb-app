const Colors = {
  light: {
    header: '#60A5FA',
    background: '#FFFFFF',
    backgroundText: '#000000'
  },
  dark: {
    header: '#1F2937',
    background: '#4B5563',
    backgroundText: '#FFFFFF'
  }
}

export default Colors