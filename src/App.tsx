import React from 'react';
import { Route, Routes } from 'react-router-dom'
import Body from './pages/Body';
import Movie from './pages/Movie';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Body />} />
        <Route path="/movie/*" element={<Movie />} />
      </Routes>
    </div>
  );
}

export default App;
